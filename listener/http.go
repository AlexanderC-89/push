package listener

import (
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/rakshazi/push/sender"
)

func listenHTTP(endpoint string, port int) {
	http.HandleFunc(endpoint, handleHTTP)
	err := http.ListenAndServe(":"+strconv.Itoa(port), nil)
	if err != nil {
		log.Println("Cannot create HTTP listener:", err)
	}
}

func handleHTTP(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("[listener/http] error parsing body: ", err)
	}
	if len(body) > 0 {
		log.Println("[listener/http] new push notificaiton", string(body))
		message := sender.Parse(body)
		go sender.Send(message)
	}
}
