package listener

import (
	"log"
	"net"
	"time"

	"github.com/go-redis/redis"
	"gitlab.com/rakshazi/push/sender"
)

func listenRedis(url string, list string) {
	config, err := redis.ParseURL(url)
	if err != nil {
		log.Println("[listener/redis] cannot start:", err)
	}
	client := redis.NewClient(config)
	for {
		data, err := client.BRPop(1*time.Second, list).Result()
		if err != nil {
			if err == redis.Nil {
				continue
			}
			if neterr, ok := err.(*net.OpError); ok {
				log.Println("[listener/redis] connection error", err)
				time.Sleep(2 * time.Second)
				if neterr.Timeout() {
					continue
				}
			}
			log.Println("[listener/redis] failed to fetch list", list)
		}
		if len(data) >= 1 {
			log.Println("[listener/redis] new push notificaiton:", data[1])
			message := sender.Parse([]byte(data[1]))
			go sender.Send(message)
		}
	}
}
