package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

// Config file structure
type Config struct {
	Http struct {
		Port     int
		Endpoint string
		Enabled  bool
	}
	Redis struct {
		Url     string
		List    string
		Enabled bool
	}
	Fcm struct {
		Token string
	}
	Apns struct {
		Gateway string
		Cert    string
		Key     string
	}
}

// CreateConfigurationFromFile Returns a new configuration loaded from a file
func CreateConfigurationFromFile(configFile string) (Config, error) {
	config := Config{
		Http: struct {
			Port     int
			Endpoint string
			Enabled  bool
		}{
			Port:     8080,
			Endpoint: "/",
			Enabled:  true,
		},
		Redis: struct {
			Url     string
			List    string
			Enabled bool
		}{
			Url:     "redis://127.0.0.1:6379/2",
			List:    "push_notifications",
			Enabled: true,
		},
		Fcm: struct {
			Token string
		}{
			Token: "",
		},
	}

	if _, err := os.Stat(configFile); !os.IsNotExist(err) {
		log.Println("Found configuration file:", configFile)

		configData, err := ioutil.ReadFile(configFile)
		if err != nil {
			return config, fmt.Errorf("error reading configuation file: %v", err)
		}

		err = json.Unmarshal(configData, &config)
		if err != nil {
			return config, fmt.Errorf("error decoding configuration: %v", err)
		}

		log.Println("Configuration loaded successfully")
	}
	return config, nil
}
